resource "aws_s3_bucket" "treino-ci-cd" {
  bucket = "treino-ci-cd-xxx-123-654-teste-davi"
  acl    = "private"
  
  tags = {
    Name        = "treino-ci-cd"
    Environment = "dev-treino"
    CreatedBy   = "Terraform"
  }
}

